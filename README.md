# README #

Brief introduction to the appliation:
What:
Estonia is in the need of a booking system for harbours, where people can book a spot at a dock in the harbour online. The web application will need a list of all available ports in the system and a view of a map of the port, where people can choose their place to dock depending on their vessel size and depth. They also need the possibility to pay on the site for the expenses of staying at the harbour. There can be an optional account creating system, where the customer can save their vessel and payment details.

Why and for whom:
Currently, the process is very old-school where you have to call in advance to check for free spaces at the port and later you need to fill a written form, adding details about your vessel and the crew. The idea would be to create a system where you can easily pre-book a dock at the port without the need to call in advance or waste paper to mark your stay. Additionally, such a system does not yet exist in Estonia, which makes it unique and useful for boat owners living in or travelling to Estonia. One side of the users would be the port captains, who would use the system to log incoming and outgoing vessels and boats, manage bookings and organize traffic based on the information from the application. The system is not meant for ferries or cargo ships, it is meant for private boat owners, who travel with their vessel in order to stay at a guest harbour as you would stay at a hotel.

Communications
We communicate with each other via Facebook messenger and slack. To contact the customer, we have agreed to communicate via email or video calls. On urgent topics, we use the phone to get clarification.

### Project specifications
The project front-end is built in Vue.js and the backend is done with node.js. For database management MySQL is used.

### Code structure
The code is divided into frontend and backend. 
The backend part will deal with the server and take care of database queries, sending them to the frontend. 
Frontend folder is that part of the application with what the user will start interacting with.
The frontend itself will be divided into pages and pages into components, which we hope to re-use thoughout the application to keep the app similar and so that the user would have something new on every page interaction-wise.
The whole structure of the project is what is called a monolith.

### Who do I talk to? ###

Project roles
Marten Türk
Project manager and engineer, scrum master and collaborates and contacts with the customer, writes necessary code.

Danver Hans Värv
Software engineer and system architect, writes code and designs the system, decides on the usage of technologies

Sarah Lannes
Software engineer and tester, writes code and tests, participates in sprint planning.

Sandra Lannes
Software engineer and tester, writes code and tests, participates in sprint planning.

Idea owner
Rene Türk Tel: +3725152926 Email: Rene@artus.ee


# Instructions for running the app:

- ###cd portbooker-backend ###
- ###npm install ###
- ###cd .. ###
- ###cd portbooker-frontend ###
- ###npm install ###
- ###npm run serve ###