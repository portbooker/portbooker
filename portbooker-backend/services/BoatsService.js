import firebase from '../firebase';
import {getUser} from "./UsersService";

// Get all of the ports

const firestore = firebase.firestore();


// Add a new boat
// eslint-disable-next-line max-len
export const addBoat = async (registryNumber, boatName, boatModel, width, length, depth) => {
    await firestore.collection('boats').doc(registryNumber).set({
        boatName: boatName,
        boatModel: boatModel,
        width: width,
        length: length,
        depth: depth,
    });
};

export const getBoat = async (boatNumber) => {
    const boat = await firestore.collection('boats').doc(boatNumber).get();
    if (boat)
        return boat.data();
    return null;

};

export const getUserBoat = async (userId) => {
    const userData = await getUser(userId);
    const boatRef = await firestore.collection('boats').doc(userData.boatNumber).get();
    if (boatRef) {
        return boatRef.data()
    }
    return null;

};
// Edit boat
export const editBoat = async (boatNumber, newData) => {
    const boat = firestore.collection('boats').doc(boatNumber);
    await boat.update({
        boatName: newData.boatName,
        boatModel: newData.boatModel,
        width: newData.width,
        length: newData.length,
        depth: newData.depth,
    });
};
