import firebase from '../firebase';

const firestore = firebase.firestore();

export class BookingsService {
    constructor(userId, placeId, startTime, endTime, price, passengers, port, paid) {
        this.userId = userId
        this.placeId = placeId
        this.startTime = startTime
        this.endTime = endTime
        this.price = price
        this.passengers = passengers
        this.port = port
        this.paid = paid
    }

    async getBookingDataObject() {
        const userRef = firestore.collection('users').doc(this.userId);

        return {
            userId: userRef,
            placeId: this.placeId,
            startTime: this.startTime,
            endTime: this.endTime,
            price: this.price,
            passengers: this.passengers,
            port: this.port,
            paid: false,
        }
    }


    async createBooking(portName) {
        const bookingsRef = firestore.collection('ports').doc(portName)
            .collection('bookings');
        const bookingData = await this.getBookingDataObject();
        await bookingsRef.doc().set(bookingData);
    }

}

export const getAllUserBookings = async (uid) => {
    const ports = await firebase
        .firestore()
        .collection("ports")
        .get();
    const promises = [];
    const previousBookings = [];
    const upcomingBookings = [];
    ports.forEach(snapshot => {
        promises.push(snapshot.ref.collection('bookings').get());
    });
    const bookings = await Promise.all(promises);
    const currentDate = new Date();
    bookings.forEach(snapshotArray => {
        snapshotArray.forEach(doc => {
            if (doc.data().userId.id.toString() === uid.toString()) {
                const date = doc.data().endTime
                if (new Date(date) > currentDate) {
                    upcomingBookings.push({
                        bookingInfo: doc.data(),
                        bookingID: doc.id
                    })
                } else {
                    previousBookings.push(doc.data())
                }
            }
        })
    });

    return {
        upcoming: upcomingBookings,
        previous: previousBookings,
    }
}

export const getPortBookings = async (portName) => {
    const bookings = [];
    await firestore.collection('ports').doc(portName).collection('bookings').get()
        .then((booking) => {
            booking.docs.forEach((document) => {
                bookings.push(document.data())
            });
        });

    return bookings;
};

export const getIncomingPortBookings = async (portName) => {
    const bookings = [];
    const currentDate = new Date();

    await firestore.collection('ports').doc(portName).collection('bookings').get()
        .then((booking) => {
            booking.docs.forEach((document) => {
                const date = document.data().endTime;
                if (new Date(date) > currentDate) {
                    bookings.push({documentId: document.id, ...document.data()})
                }
            });
        });

    return bookings;
}

export const editBooking = async (portName, documentId, newData) => {
    const bookingRef = firestore.collection('ports').doc(portName)
        .collection('bookings');

    await bookingRef.doc(documentId).update({...newData});
}

export const deleteBooking = async (portName, documentId) => {
    await firebase.firestore().collection("ports").doc(portName).collection('bookings').doc(documentId).delete();
}

