import firebase from '../firebase';

const firestore = firebase.firestore();

// Add new captain
// eslint-disable-next-line max-len
export const addCaptain = async (fullName, email, password, phoneNumber, portName) => {
    await firestore.collection('captains').doc(fullName).set({
        phoneNumber: phoneNumber,
        email: email,
        password: password,
        portName: portName,
    });
};
