import firebase from "../../../../../portbooker-backend/firebase";
import {getUserBoat} from "../../../../../portbooker-backend/services/BoatsService";
import {getPortBookings} from "../../../../../portbooker-backend/services/BookingsService";

export const boatFitsInPlace = async (placeData) => {
    const currentUser = firebase.auth().currentUser.uid;
    const boatInformation = await getUserBoat(currentUser);
    const { width: placeWidth, length: placeLength, depth: placeDepth } = placeData;
    const { width: boatWidth, length: boatLength, depth: boatDepth } = boatInformation;

    return placeWidth >= boatWidth && placeLength >= boatLength && placeDepth >= boatDepth;
};

export const placeAlreadyBooked = async (placeData, startDate, endDate, portName) => {
    const bookings = await getPortBookings(portName);
    const filteredBookings = bookings.filter((booking) => booking.placeId === placeData.id);

    for (const booking of filteredBookings) {
        const { startTime, endTime } = booking;
        const bookingStartDate = new Date(startTime);
        const bookingEndDate = new Date(endTime);
        const selectedStartDate = new Date(startDate);
        const selectedEndDate = new Date(endDate);

        if (selectedStartDate < bookingEndDate || (selectedEndDate > bookingStartDate && selectedEndDate < bookingEndDate)) {
            return true;
        }
    }
    return false;
};



