import {createApp} from 'vue'
import App from './App.vue'
import axios from 'axios';
import firebase from "../../portbooker-backend/firebase";
import createPersistedState from "vuex-persistedstate";
import router from './router'
import Vuex from 'vuex'

require("firebase/auth");


let app;

const store = new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
    state: {
        port: "",
        name: "",
        refreshPort: true
    }, mutations: {
        change(state, portName) {
            state.port = portName;
        },
        updateName(state, userName) {
            state.name = userName;
        }
    }
});

firebase.auth().onAuthStateChanged(() => {
    if (!app) {

        app = createApp(App);
        app.use(router);
        app.use(store);
        app.mount('#app');
        app.provide('$axios', axios);
    }
})
