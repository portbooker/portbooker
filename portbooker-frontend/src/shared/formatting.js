import moment from "moment";

function formatDate(date) {return moment(date, 'YYYY-MM-DD').format('DD.MMM.YYYY');}
function formatDateToDefaultValue(date){
    return moment(new Date(date)).format('YYYY-MM-DD').toString()
}

export {formatDate, formatDateToDefaultValue}