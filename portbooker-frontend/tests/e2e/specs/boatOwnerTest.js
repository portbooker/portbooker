describe('boatOwnerTest', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('Welcome boat owner!')
  })

  it('From login page to browse ports page', () => {
    cy.visit('/')
    cy.get('#email').type("cypress@gmail.com")
    cy.get('#logInPassword').type("cypress")
    cy.get('button').click()
    cy.wait(300)
    cy.contains('Ports near me')
  })

  it('Opens module from browse ports', () => {
    cy.visit('/browse')
    cy.get('.portImage').first().click()
    cy.contains('Address')
  })

  it('From browse ports to booking', () => {
    cy.visit('/browse')
    cy.get('.portImage').first().click()
    cy.contains('Address')
    cy.get('.white-bold-text').click()
    cy.contains('Pick dates to get prices')
  })

  it('Successful booking', () => {
    cy.visit('/browse')
    cy.get('.portImage').first().click()
    cy.contains('Address')
    cy.get('.white-bold-text').click()
    cy.contains('Pick dates to get prices')
    cy.get('#startdate').type("2030-10-10")
    cy.get('#enddate').type("2030-11-10")
    cy.get('#submit-button').click()
    cy.get('.false').first().click()
    cy.contains('Booking details')
    cy.get('#numberOfPassengers').type("9")
    cy.get('.payButton').first().click()
    cy.contains('Booking has been successfully made')
  })

  it('Account info', () => {
    cy.visit('/account')
    cy.contains("Account details")
    cy.contains("Cypress Testing")
    cy.contains("55555555")
    cy.contains("submarine")
    cy.contains("Yellow submarine")
    cy.contains("RUS999")
  })

  it('My bookings', () => {
    cy.visit('/mybookings')
    cy.contains('Upcoming Bookings')
    cy.contains('Previous Bookings')
  })

  it('Delete a booking', () => {
    cy.visit('/mybookings')
    cy.wait(300)
    cy.contains('Upcoming Bookings')
    cy.get('.delete').first().click()
    cy.get("#confirm").click()
    cy.contains('Upcoming Bookings')
  })

  it('Log out', () => {
    cy.visit('/browse')
    cy.contains('Log out').click()
    cy.contains("Sign in to your account")
  })

  it('cant login with non existing user', () => {
    cy.visit('/')
    cy.get('#email').type("cypress@portbooker.com")
    cy.wait(200)
    cy.get('#logInPassword').type("PoRtB00KeRxxx")
    cy.wait(200)
    cy.on('window:alert', (str) => {
      expect(str).to.equal(`Firebase: There is no user record corresponding to this identifier. " +
        "The user may have been deleted. (auth/user-not-found).`)
    })
    cy.contains('Ports near me').should('not.exist')
  })
})
